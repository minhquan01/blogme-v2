import NavigationSidebar from "@/components/navigation/navigation.sidebar";
import { ChildrenProps } from "@/lib/interface";
import React from "react";

const ChatLayout = ({ children }: ChildrenProps) => {
  return (
    <div className="h-full">
      <div className=" md:flex h-full w-[72px] z-30 flex-col fixed inset-y-0">
        <NavigationSidebar />
      </div>
      <main className="md:pl-[72px] h-full">{children}</main>
    </div>
  );
};

export default ChatLayout;
